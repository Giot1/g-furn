import ContactForm from '../components/ContactForm'
import style from '../styles/Contact.module.css'
import Meta from '../components/Meta'

const Contact = () => {
  return (  
    <>
    <Meta title={"Επικοινωνία"} description={"Στείλτε μας οποιαδήποτε απορία σας"} keywords={"Επικοινωνία,Γιωτάκης,μεταφορά,αποστολή,προϊόντα"}/>
    <section className={style["container"]}>
      <h1 className={style["page-title"]}>Επικοινωνία</h1>
      <p className={style["page-title-text"]}>Τα προιόντα της Giotakis furniture έχουν την δυνατότητα αποστολής σε οποιαδήποτε χώρα η πόλη λόγω του ιδιαίτερου, λεπτομερούς και ανθεκτικού πακεταρίσματος μας.</p>
      <div className={style["info-form-container"]}>
        <div className={style["info-map"]}>
          <h2 className={style["information-title"]}>Πληροφορίες</h2>
          <ul className={style["information-list"]}>
            <li className={style["information-list-item"]}><strong>Διεύθυνση</strong>: Νίκαια - Λάρισας</li>
            <li className={style["information-list-item"]}><strong>Mail</strong>: info@epiplogiotakis.gr</li>
            <li className={style["information-list-item"]}><strong>Τηλέφωνο</strong>: 2410921587</li>
            <li className={style["information-list-item"]}><strong>Κινητό</strong>: 6983594372</li>
            <li className={style["information-list-item"]}><strong>Fax</strong>: 6985773721</li>
          </ul>
          <div className={style["map-container"]}>
            <iframe width="600" height="450" style={{border:0}} loading="lazy" allowFullScreen title="map" aria-hidden="false" tabIndex="0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJQfjbsKqFWBMRNk6pY-tC2ic&key=AIzaSyC0a_BsdxdbLAQTrPNR7OWBjpc4oi3c5tk
            "></iframe> 
          </div>
        </div>
        <ContactForm />
      </div>
     </section>
    </>

  );
}
 
export default Contact;
import style from '../styles/Materials.module.css'
import Meta from '../components/Meta'
import Image from 'next/image'

const Materials = () => {
  return ( 
    <>
    <Meta title={"Υλικά"} description={"Τα υλικά που χρησιμοποιύμε για την κατασκευή των επίπλων της εταιρίας μας."} keywords={"Δρυς-ντος,Είδη-ξυλείας,Ξύλο,Υλικά-1ης-ύλης,Είδη-κεραμικών,Ballet,Nero-Ardi,Pietrasanda,Saint-Denis-Verde,Silk,Supreme Dark"}/>
    <div className={style["page-wrapper"]}>
      <div className={style["title-container"]}>
        <h1 className={style["title"]}>Υλικά 1ης ύλης</h1>
        <p className={style["title-text"]}>Όλα χειροποίητα , με υλικά πρώτης διαλογής , τα προιόντα μας αντέχουν στο χρόνο δίνωντας στο χώρο σας το συναίσθημα του παντοτινού.</p>
      </div>
      <div className={style["container"]}>
        <div className={style["section-container"]}>
        <section className={style["section"]}>
          <h2 className={style["section-title"]}>Είδη ξυλείας</h2>
          <div className={style["material-container"]}>
            <div className={style["item"]}>
              <Image src="/materials/oak-natural.png" alt='square picture of oak dos ' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Δρυς ντος (φαρδύνερο)</h3>
            </div>
            <div className={style["item"]}>
              <Image src="/materials/oak-rustic.png" alt='square picture of oak rustik ' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Δρυς ρουστίκ</h3>
            </div>
            <div className={style["item"]}>
              <Image src="/materials/american-walnut.jpg" alt='square picture of american walnut wood' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Αμερικανική καρυδιά</h3>
            </div>
          </div>
        </section>
        <hr></hr>
        <section className={style["section"]}>
          <h2 className={style["section-title"]}>Είδη κεραμικών</h2>
          <div className={style["material-container"]}>
            <div className={style["item"]}>
              <Image src="/materials/Ballet.jpg" alt='square picture of ceramic named Ballet' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Ballet</h3>
            </div>
            <div className={style["item"]}>
              <Image src="/materials/nero-ardi.jpg" alt='square picture of ceramic named Nero-Ardi' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Nero-Ardi</h3>
            </div>
            <div className={style["item"]}>
              <Image src="/materials/PIETRASANTA.jpg" alt='square picture of ceramic named Pietrasanda' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Pietrasanda</h3>
            </div>
            <div className={style["item"]}>
              <Image src="/materials/saint-denis-verde-.jpg" alt='square picture of ceramic named Saint-Denis-Verde' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Saint-Denis-Verde</h3>
            </div>
            <div className={style["item"]}>
              <Image src="/materials/SILK.jpg" alt='square picture of ceramic named Silk' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Silk</h3>
            </div>
            <div className={style["item"]}>
              <Image src="/materials/Supreme_Dark.jpg" alt='square picture of ceramic named Supreme Dark' width={250} height={250} layout='intrinsic'/>
              <h3 className={style["material-title"]}>Supreme Dark</h3>
            </div>
          </div>
        </section>
        </div>
      </div>
    </div>
    </>
   );
}
 
export default Materials;
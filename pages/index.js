import React, {useRef,useEffect,useState} from 'react'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Meta from '../components/Meta'

export default function Home() {

  const node = useRef({})
  const ndc = node.current;
  const [startPos,setStartPos] = useState(0);
  const [slider,setSlider] = useState(1);
  const [targetDot,setTargetDot] = useState(1);
  
  // Arrange the Slides next to one another
  const setSlidePosition = () => {
    const slideWidth = ndc.li1.getBoundingClientRect().width;
    ndc.li2.style.left = slideWidth + 50 + 'px';
    ndc.li3.style.left = slideWidth * 2 + 150 +'px';
  }

  const handleClickRight = () => {
    if(slider>=1 && slider<3){
      setSlider(slider+1);
    } else {
      return;
    }
  }

  const handleClickLeft = () => {
    if(slider>1 && slider<=3){
      setSlider(slider-1);
    } else {
      return;
    }
  }

  const moveTheSlide = (num) => {
    if (num === 1){
      const moveAmount = 0+'%';
      ndc.track.style.transform = `translateX(-${moveAmount})` ;
    } else if(num === 2) {
      const moveAmount = ndc.li2.style.left;
      ndc.track.style.transform = `translateX(-${moveAmount})` ;
    } else if(num === 3) {
      const moveAmount = ndc.li3.style.left;
      ndc.track.style.transform = `translateX(-${moveAmount})` ;
    }
  }

  const arrowShowHide = () => {
    if (slider===2){
      ndc.btnR.style.opacity = '1';
      ndc.btnL.style.opacity = '1';
    } else if(slider === 1) {
      ndc.btnL.style.opacity = '0.3';
      ndc.btnR.style.opacity = '1';
    } else if(slider === 3) {
      ndc.btnR.style.opacity = '0.3';
      ndc.btnL.style.opacity = '1';
    }
  }

  const touchStart = (event) => {
    setStartPos(event.touches[0].clientX);
  }


  const touchEnd = (event) => {
    const end = event.changedTouches[0].clientX;
    const position = end - startPos;
    if (position >= 0 && Math.abs(position)>=25) {
      handleClickLeft();
    } else if( position<=0 && Math.abs(position)>=25) {
      handleClickRight();
    }
  }



 
  useEffect(() => {
    
    window.screen.width
  
    if(window.screen.width<800) {
      ndc.video.removeAttribute('autoPlay');
    } else {
      ndc.video.setAttribute('autoPlay', "")
    }
    
  },[]);

  useEffect(() => {
    setSlidePosition();
    arrowShowHide();
    moveTheSlide(slider);
    setTargetDot(slider);
  }, [slider]);

  useEffect(() => {
    setSlider(targetDot);
  }, [targetDot]);

  
  
  return (
    <>
     <Meta title={"Έπιπλο"} description={"Η εταιρία επίπλων Γιωτάκης που εδρεύει στην Λάρισα δημιουργεί το τελικό προιόν  με υλικά φιλικά προς το περιβάλλον με υψηλή ποιότητα και design.Το ευρύ φάσμα τιμών των προιόντων μας καλύπτει κάθε σας ανάγκη"} keywords={"Giotakis,funriture,έπιπλο,Νίκαια,Λάρισα,Θεσσαλία,ελλάδα,μοντέρνο,μπουφέδες,τραπεζαρίες,τραπεζάκια-σαλονιού"}/>
      <video ref={el=>ndc["video"]=el} className={styles["home-video"]} src="/video/video.mp4" controls controlsList="nodownload" muted preload='metadata'></video>
      <div className={styles["home-wrapper"]}>
        <div className={styles["container"]}>
          <header className={styles["page-title"]}>
            <h1>Giotakis Furniture</h1>
            <p>Μετά απο μία διαδικασία κατανόησης των αναγκών και της καθημερινότητάς σας, η εταιρία επίπλων Γιωτάκης που εδρεύει στην Λάρισα δημιουργεί το τελικό προϊόν  με υλικά φιλικά προς το περιβάλλον με υψηλή ποιότητα και design.Το ευρύ φάσμα τιμών των προϊόντων μας καλύπτει κάθε σας ανάγκη. Η κατανόηση των δυνατοτήτων  σας είναι για μας βασικό μας πλεονέκτημα.</p>
          </header>
          <section className={styles["products-container"]}>
            <h2 className={styles["products-title"]}>Τα προϊόντα μας</h2>
            <div className={styles["carousel"]} onTouchStart={(e)=>touchStart(e)} onTouchEnd={(e)=>touchEnd(e)}   onContextMenu={(e) => {e.preventDefault() 
            e.stopPropagation()}}>
              <button ref={el => ndc['btnL']= el} className={`${styles["carousel__button"]} ${styles["carousel__button--left"]}`} onClick={() => handleClickLeft()}><i className="fas fa-chevron-left" aria-label="previous-image"></i></button>
              <div className={styles["carousel__track-container"]}>
                <ul ref={el=>ndc['track']=el} className={styles["carousel__track"]}>
                  <li ref={el => ndc['li1'] = el} className={`${styles["dining-tables"]} ${styles["carousel__slide"]} ${styles["products-grid"]}`}>
                    <div className={styles["dining-tables-image-wrapper"]}>
                      <Image src="/dining-table-Giotakis.jpg" layout="fill" objectFit="contain" alt="dining-table" priority/>
                  </div>
                    <article className={styles["dining-tables-text"]}>
                      <h3>Τραπεζαρίες</h3>
                      <p className={styles["product-text"]}>Σπίτι η πιο οικία λέξη μετά την αγάπη, γι'αυτό κι'εμείς δημιουργούμε προϊόντα υψηλών προδιαγραφών σχεδιασμένα να πληρούν τις καθημερινές σας απαιτήσεις.</p>
                      <Link href="/products/dining-tables"><a className={styles["products-button"]}>Πάθος</a></Link>
                    </article>
                  </li>
                  <li ref={el => ndc['li2'] = el} className={`${styles["coffee-tables"]} ${styles["carousel__slide"]} ${styles["products-grid"]}`}>
                    <div className={styles["coffee-tables-image-wrapper"]}>
                      <Image src="/coffee-table-Giotakis.jpg" layout="fill" objectFit="contain" alt="coffee-table" priority/>
                    </div>
                      <article className={styles["coffee-tables-text"]}>
                        <h3>Τραπεζάκια Σαλονιού</h3>
                        <p className={styles["product-text"]}>Χρησιμοποιώντας υλικά φιλικά προς το περιβάλλον η Giotakis furniture υπόσχεται την άνεση και τη ποιότητα στο χώρο σας.</p>
                        <Link href="/products/coffee-tables"><a className={styles["products-button"]}><span className={styles["btn-border"]}>Δημιουργία</span></a></Link>
                      </article>
                  </li>
                  <li ref={el => ndc['li3'] = el} className={`${styles["buffets"]} ${styles["carousel__slide"]} ${styles["products-grid"]}`}>
                    <div className={styles["buffets-image-wrapper"]}>
                     <Image src="/buffet-Giotakis.jpg" layout="fill" objectFit="contain" alt="buffet" priority/>   
                    </div>
                      <article className={styles["buffets-text"]}>
                        <h3>Μπουφέδες</h3>
                        <p className={styles["product-text"]}>Φτιαγμένα με μεγάλη έμφαση στις ανάγκες σας , απο το σχεδιασμό μέχρι τη κατασκευή , τα προϊόντα μας σας υπόσχονται άνεση και διάρκεια.</p>
                        <Link href="/products/buffet"><a className={styles["products-button"]}>Έκφραση</a></Link>
                      </article>
                  </li>
                </ul>
              </div>
              <button ref={el => ndc['btnR'] = el} className={`${styles["carousel__button"]} ${styles["carousel__button--right"]}`} 
              onClick={() => handleClickRight()} ><i className="fas fa-chevron-right" aria-label="next-image"></i></button>
              <div  className={styles["carousel__nav"]}>
                <button type="button" aria-label="first-image" ref={el=> ndc['dot1'] = el} onClick={()=> setTargetDot(1)} className={`${styles["carousel__indicator"]} ${(slider===1) ? styles["current-indicator"]: ""}`}></button>
                <button type="button" aria-label="second-image" ref={el=> ndc['dot2'] = el} onClick={()=> setTargetDot(2)} className={`${styles["carousel__indicator"]} ${(slider===2)?styles["current-indicator"]: ""}`}></button>
                <button type="button" aria-label="third-image" ref={el=> ndc['dot3'] = el} onClick={()=> setTargetDot(3)} className={`${styles["carousel__indicator"]} ${(slider===3)?styles["current-indicator"]: ""}`}></button>
              </div>
            </div>
          </section>
        </div>
          <div className={styles["horizontal-line"]}></div>
      </div>
      
    </>
  )
}

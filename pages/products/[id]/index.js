import CardList from '../../../components/CardList'
import Meta from '../../../components/Meta'
import data from '/products.json'

export const getStaticPaths = async () => {
 const paths = data.map( data => {
    return {params : {id: data.id}}
  })

  return {
    paths,
    fallback: false
  }
}

export const getStaticProps = async (context) => {
  const filteredData = data.filter(data=> data.id===context.params.id)
   
  return {
    props: {filteredData}
  }
}



const Products = ({ filteredData }) => {
  const filtered = filteredData[0];
  return ( 
    <>
      <Meta title={filtered.title} keywords={filtered.keywords} description={filtered.description}/>
      <CardList catalog={filtered} />
      
    </>    
   );
}
 
export default Products;
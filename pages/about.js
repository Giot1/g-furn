import Meta from '../components/Meta'
import Image from 'next/image'
import style from '../styles/About.module.css'

const About = () => {
  return ( 
    <> 
   <Meta title={"Εταιρία"} description={"Ξεκινώντας απο ένα μικρό συνοικιακο εργαστήριο χειροποιητων επίπλων το 1999 , η επιχείρηση Giotakis furniture, με αγάπη και μεράκι για το ποιοτικο έπιπλο , έφτασε να δημιουργήσει την υπερσυγχρονη μονάδα 300 τ.μ. στο κάμπο της Λάρισας..."} keywords={"Giotakis,έπιπλο,furniture,Έπιπλο-Γιωτάκης,εταιρία,επιχείρηση,υπερσυγχρονα,μηχανηματα"}/>
      <div className={style["container"]}>
        <div className={style["image-container"]}>
        <Image src='/company-image.jpg' width={1280} height={720} alt='giotakis company image' layout='intrinsic'/>
        </div>
        <article className={style["article"]}>
          <h2 className={style["article-title"]}>Εταιρία Γιωτάκης</h2>
          <p className={style["article-text"]}>Ξεκινώντας απο ένα μικρό συνοικιακό εργαστήριο χειροποιητων επίπλων το 1999 , η επιχείρηση Giotakis furniture, με αγάπη και μεράκι για το ποιοτικό έπιπλο , έφτασε να δημιουργήσει την υπερσύγχρονη μονάδα 300 τ.μ. στο κάμπο της Λάρισας διαθέτωντας πλέον ορισμένα απο τα τελευταίας τεχνολογίας υπερσύγχρονα μηχανήματα κατεργασίας ξύλου , με το ποιοτικό χειροποίητο έπιπλο να είναι η καθημερινή μας δέσμευση.
            Ο χαρακτήρας καθ'ενός απο εσάς, που επιλέγει και εμπιστεύεται την εταιρία μας , είναι αυτό που χαρακτηρίζει το κάθε μας προϊόν.
            Δεν δημιουργούμε αντικείμενα αλλα αξίες ζωής.
          </p>
        </article>
      </div>
    </>
   );
}
 
export default About;
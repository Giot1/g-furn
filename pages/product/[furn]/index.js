import FurnDetails from "../../../components/FurnDetails";
import Meta from '../../../components/Meta'
import data from '/productDetails.json'
import { useEffect } from 'react'
import * as fbq from '../../../lib/fpixel'


export const getStaticPaths = async () => {


  const paths = data.map(data => {
    return {params: {furn: data.id}}
  })

  return {
    paths,
    fallback: false
  }
}

export const getStaticProps = async (context) => {
 
  const filteredData = data.filter(data=> data.id === context.params.furn);

  return {
    props: {filteredData}
  }
}

const FurnitureDetails = ({ filteredData }) => {
  const filtered = filteredData[0];

  useEffect ( () => {
    fbq.event('ViewContent', { content_ids: [`${filtered.id}`], content_category: `${filtered.category}` })
  },[]);
  
  return ( 
  <>

    <Meta title={filtered.id} keywords={filtered.keywords} description={filtered.text}/>
    <FurnDetails details={filtered} />
    
  </>
 );
}
 
export default FurnitureDetails;
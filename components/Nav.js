import React, {useState,useEffect,useRef} from 'react'
import Link from 'next/link'
import navStyles from '../styles/Nav.module.css'
import Image from 'next/image'



const Nav = () => {
  const node = useRef();
  const links = useRef({});
  const linkCur = links.current;
  // Toggle Navigation
 const [open,setOpen] = useState(false);
//  Toggle Dropdown
 const [dropOpen,setDropOpen] = useState(false);
//  Target NavLink
const [navLink,setNavLink] = useState(null);
const [isBright,setIsBright] = useState(false);


  // Click outside dropdown
 const handleClickOutside = (e) => {
  console.log("clicking anywhere");
  if (node.current.contains(e.target)) {
    // inside click
    return;
  }
  // outside click
  setDropOpen(false);
};

// Getting navLinks into an array
  useEffect(() => {
  linkCur["arr"] = Array.from(linkCur.ul.children);
  console.log(linkCur.arr)
}, []);
  
 useEffect(() => {
  if (dropOpen) {
    document.addEventListener("mousedown", handleClickOutside);
  } else {
    document.removeEventListener("mousedown", handleClickOutside);
  }

  return () => {
    document.removeEventListener("mousedown", handleClickOutside);
  };
}, [dropOpen]);
 

// Change Brighness when hovering over navLink
  useEffect(() => {
    if(isBright === true) {
      for(let i=0; i<linkCur.arr.length; i++) {
        if(linkCur.arr[i] !== linkCur.arr[navLink]) {
          linkCur.arr[i].style.filter = "brightness(75%)";
        }
      }
    } else {
      for(let i=0; i<linkCur.arr.length; i++) {
        if(linkCur.arr[i] !== linkCur.arr[navLink]) {
          linkCur.arr[i].style.filter = "brightness(100%)";
        }
      }
    }
  },[isBright]);


    return ( 
      <header className={navStyles.header}>
        <nav className={navStyles.container}>
          <Link href='/'><a className={navStyles.logo} onClick={()=>{setOpen(false); setDropOpen(false);}}><Image src='/logo.png' width={180} height={45} layout='intrinsic' alt='logo'/></a></Link>
          <ul ref={el=>linkCur["ul"]=el} className={`${navStyles.links} ${open ? navStyles.navActive : ""}`}>
            <li onMouseEnter={() => {setNavLink(0); setIsBright(true);}} onMouseLeave={() => {setNavLink(null); setIsBright(false);}} className={` ${open ? (navStyles.link +' ' + navStyles.delay1): "" }`} onClick={()=>{setOpen(false); setDropOpen(false);}}><Link href='/'><a>Αρχική</a></Link></li>
            <li onMouseEnter={() => {setNavLink(1); setIsBright(true);}} onMouseLeave={() => {setNavLink(null); setIsBright(false);}} className={` ${open ? (navStyles.link +' ' + navStyles.delay2): "" }`} onClick={()=>{setOpen(false); setDropOpen(false);}}><Link href='/about'><a>Εταιρία</a></Link></li>
            <li onMouseEnter={() => {setNavLink(2); setIsBright(true);}} onMouseLeave={() => {setNavLink(null); setIsBright(false);}} className={` ${open ? (navStyles.link +' ' + navStyles.delay3): "" }`} onClick={()=>{setOpen(false); setDropOpen(false);}}><Link href='/materials'><a>Υλικά</a></Link></li>
            <li onMouseEnter={() => {setNavLink(3); setIsBright(true);}} onMouseLeave={() => {setNavLink(null); setIsBright(false);}} ref={node} className={`${navStyles.hasDropdown} ${open ? (navStyles.link +' ' + navStyles.delay4 ): "" }`} >
              <button type="button" className={navStyles.btn} onClick={()=>setDropOpen(!dropOpen)}>Προϊόντα <i className={`${dropOpen?"fas fa-chevron-up" :"fas fa-chevron-down"}`}></i></button>
               {/* Dropdown */}
              <ul className={`${navStyles.dropdown} ${dropOpen ? navStyles.dropOpen : ""}`} >
                <li onClick={()=>{setOpen(false); setDropOpen(false);}} className={navStyles["link-animation"]}><Link href="/products/dining-tables"><a>Τραπεζαρίες</a></Link></li>
                <li onClick={()=>{setOpen(false); setDropOpen(false);}} className={navStyles["link-animation"]}><Link href="/products/coffee-tables"><a>Τραπεζάκια</a></Link></li>
                <li onClick={()=>{setOpen(false); setDropOpen(false);}} className={`${navStyles.specialItems} ${navStyles["link-animation"]}`}><Link href="/products/buffet"><a>Μπουφέδες & more</a></Link></li>
              </ul>
            </li>
            <li onMouseEnter={() => {setNavLink(4); setIsBright(true);}} onMouseLeave={() => {setNavLink(null); setIsBright(false);}} className={`${navStyles["link-animation"]} ${open ? (navStyles.link +' ' + navStyles.delay5): "" }`} onClick={()=>{setOpen(false); setDropOpen(false);}}><Link href='/contact'><a>Επικοινωνία</a></Link></li>
            {/* Burger */}
          </ul>
          <div className={navStyles.burger} onClick={()=>{setOpen(!open); setDropOpen(false);}}>
            <div className={`${navStyles.line1} ${open ? navStyles.toggleLine1 : ""}`}></div>
            <div className={`${navStyles.line2} ${open ? navStyles.toggleLine2 : ""}`}></div>
            <div className={`${navStyles.line3} ${open ? navStyles.toggleLine3 : ""}`}></div>
          </div>
        </nav>
      </header>
     );
}
 
export default Nav;
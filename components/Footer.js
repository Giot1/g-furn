import style from '../styles/Footer.module.css'


const Footer = () => {
  const getYear = () => {
    return new Date().getFullYear()
  }

  return ( 
    <footer>
  
      <div className={style["footer-container"]}>
        <section className={style["information-container"]}>
          <h3 className={style["information-title"]}>Πληροφορίες</h3>
          <ul className={style["information-list"]}>
            <li className={style["information-list-item"]}><strong>Διεύθυνση:</strong> Νίκαια - Λάρισας</li>
            <li className={style["information-list-item"]}><strong>Mail:</strong> info@epiplogiotakis.gr</li>
            <li className={style["information-list-item"]}><strong>Τηλέφωνο:</strong> 2410921587</li>
            <li className={style["information-list-item"]}><strong>Κινητό:</strong> 6983594372</li>
            <li className={style["information-list-item"]}><strong>Fax:</strong> 6985773721</li>
          </ul>
        </section>
        <div>
          <section className={style["social-media"]}>
            <h3 className={style["social-media-title"]}>Ακολουθήστε μας</h3>
            <div className={style["social-media-icons"]}>
              <a href="https://www.facebook.com/EpiploGiotakis" target="_blank" title="follow us on facebook" rel="noopener"><i className={`fab fa-facebook ${style["facebook"]}`}></i></a>
              <a href="https://www.instagram.com/giotakis_furniture/" target="_blank" title="follow us on instagram" rel="noopener"><i className={`fab fa-instagram ${style["instagram"]}`}></i></a>
            </div>
          </section>
          <a href="https://www.google.com/maps/place/%CE%93%CE%B9%CF%89%CF%84%CE%B1%CE%BA%CE%B7%CF%82.%CE%91.%CE%95%CF%80%CE%B9%CF%80%CE%BB%CE%B1/@39.570674,22.4770578,19z/data=!4m13!1m7!3m6!1s0x0:0x0!2zMznCsDM0JzE0LjQiTiAyMsKwMjgnMzkuNCJF!3b1!8m2!3d39.5706667!4d22.4776111!3m4!1s0x135885aab0dbf841:0x27da42eb63a94e36!8m2!3d39.570673!4d22.477605" target="_blank" title="Find us in google maps" rel="noopener" className={style["location"]}><i className={`fas fa-map-marker-alt ${style["location-icon"]}`}></i> Βρείτε μας στο Google Maps</a>
          <div className={style["copyright"]}>
            <small>&copy; Giotakis {getYear()} All rights reserved.</small>
          </div>
        </div>
      </div>      
    </footer>
   );
}
 
export default Footer;
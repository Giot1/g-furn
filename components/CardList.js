import Card from "./Card";
import Link from 'next/link'
import styles from '../styles/CardList.module.css'

const CardList = ({ catalog }) => {
  return (  
    <>
    <section className={styles["container"]}>
      <div className={styles["title-container"]}>
        <h1 className={styles["title"]}>{catalog.title}</h1>
        <p className={styles["title-text"]}>{catalog.text}</p>
      </div>
      <div className={styles.grid}>
        {catalog.products.map( data => (
          <Link href={`/product/${data.id}`} key={data.id}><a><Card data={data} key={data.id} /></a></Link>
        ))}
      </div>
      </section>
    </>
  );
}
 
export default CardList;
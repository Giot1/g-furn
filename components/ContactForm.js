import { useState } from 'react'
import axios from 'axios'
import style from '../styles/Contact.module.css'   
import * as fbq from '../lib/fpixel'

const ContactForm = () => {
  const handleClick = () => {
    fbq.event('SubmitApplication', {} )
  }
  const [status, setStatus] = useState({
    submitted: false,
    submitting: false,
    info: { error: false, msg: null },
  })
  const [inputs, setInputs] = useState({
    name: '',
    email: '',
    message: '',
  })
  const handleServerResponse = (ok, msg) => {
    if (ok) {
      setStatus({
        submitted: true,
        submitting: false,
        info: { error: false, msg: msg },
      })
      setInputs({
        email: '',
        message: '',
      })
    } else {
      setStatus({
        info: { error: true, msg: msg },
      })
    }
  }
  const handleOnChange = (e) => {
    e.persist()
    setInputs((prev) => ({
      ...prev,
      [e.target.id]: e.target.value,
    }))
    setStatus({
      submitted: false,
      submitting: false,
      info: { error: false, msg: null },
    })
  }
  const handleOnSubmit = (e) => {
    e.preventDefault()
    setStatus((prevStatus) => ({ ...prevStatus, submitting: true }))
    axios({
      method: 'POST',
      url: 'https://formspree.io/f/xoqydqkj',
      data: inputs,
    })
      .then((response) => {
        handleServerResponse(
          true,
          'Σας ευχαριστούμε, το μήνυμά σας έχει σταλεί'
        )
      })
      .catch((error) => {
        handleServerResponse(false, error.response.data.error)
      })
  }
  return (
    <>
    <div className={style["form-container"]}>
      <h2 className={style["form-title"]}>Για οποιαδήποτε πληροφορία συμπληρώστε την παρακάτω φόρμα</h2>
      <p className={style["form-title-text"]}>Όλα τα πεδία είναι υποχρεωτικά</p>
      <form onSubmit={handleOnSubmit}>
        <div className={style["form-input"]}>
          <label className={style["form-label"]} htmlFor="name">Όνομα</label>
          <input
            className={`${style["name"]} ${style["input"]}`}
            id="name"
            type="text"
            name="name"
            onChange={handleOnChange}
            required
            value={inputs.name}
          />
        </div>
        <div className={style["form-input"]}>
          <label htmlFor="email" className={style["form-label"]}>Email</label>
          <input
            className={`${style["email"]} ${style["input"]}`}
            id="email"
            type="email"
            name="_replyto"
            onChange={handleOnChange}
            required
            value={inputs.email}
          />
        </div>
        <div className={style["form-input-textarea"]}>
          <label htmlFor="message" className={style["form-label"]}>Το μήνυμά σας</label>
          <textarea
            className={`${style["text-area"]} `}
            id="message"
            name="message"
            rows='5'
            onChange={handleOnChange}
            required
            value={inputs.message}
          />
        </div>
        <button className={style["button"]} type="submit" disabled={status.submitting} onClick={handleClick}> 
          {!status.submitting
            ? !status.submitted
              ? 'Αποστολή'
              : 'Εστάλη'
            : 'Περιμένετε...'}
        </button>
      </form>
      {status.info.error && (
        <div className={style["error"]}>Error: {status.info.msg}</div>
      )}
      {!status.info.error && status.info.msg && <p className={style["info-message"]}>{status.info.msg}</p>}
      </div>
    </>
  )
}

export default ContactForm;
import Head from 'next/head'

const Meta = ({title, keywords, description}) => {
  return ( 
    <Head>
      <meta content="IE=edge" httpEquiv="X-UA-Compatible" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
      <title>{`${title} - Giotakis`}</title>
      <meta property="og:title" content={`${title} - Giotakis`} /> 
      <meta property="og:description" content={description} />
      <meta name="description" content={description} />
      <meta name="keywords"  content ={keywords}/>
    
    </Head>
  );
}


 
export default Meta;
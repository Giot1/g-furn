import React, {useRef,useEffect,useState} from 'react'
import style from '../styles/FurnDetails.module.css'
import Image from 'next/image'
import Link from 'next/link'

const FurnitureTemplate = ({ details }) => {

  
  const node = useRef({});
  const ndc = node.current;
  const [slider,setSlider] = useState(0);
  const [carouselNav,setCarouselNav] = useState(0);
  const [startPos, setStartPos] = useState(0);
  


  // get first slide position and position the others
 const getSlidePosition = () => {
    const slideWidth = ndc.li0.getBoundingClientRect().width;
    for(let i=1; i<details.photo.length; i++){
      ndc[`li${i}`].style.left = (slideWidth * i)  +'px';
    }
  }

  const handleClickLeft = () => {
    if(slider!==0){
      setSlider(slider-1);
    }else {
      return;
    }
  }

  const handleClickRight = () => {
    if(slider!==details.photo.length-1) {
      setSlider(slider+1);
    }else {
      return;
    }
  }

  const moveTheSlide = (num) => {
    let moveAmount = ndc[`li${num}`].style.left;
    if(num===0){
      moveAmount = "0%"
    }
    ndc.track.style.transform = `translateX(-${moveAmount})`;
  }

  const arrowShowHide = (num) => {
    ndc.btnL.style.opacity = '1';
    ndc.btnR.style.opacity = '1';

    if(num===0){
      ndc.btnL.style.opacity = '0.3';
    } else if(num===details.photo.length-1){
      ndc.btnR.style.opacity = '0.3';
    } 
      
  }

  const pickSlide = (e) => {
    const targetPic = e.target.closest('button');
    // const arr = Array.from(ndc.carouselNav.children);
    if(!targetPic){
      return;
    }
   const foundIt =  ndc.arr.findIndex(num=> num === targetPic );
   setCarouselNav(foundIt);
  }

  const touchStart = (event) => {
    setStartPos(event.touches[0].clientX);
  }

  const touchEnd = (event) => {
    const end = event.changedTouches[0].clientX;
    const position = end - startPos;
    if (position >= 0 && Math.abs(position)>=25) {
      handleClickLeft();
    } else if( position<=0 && Math.abs(position)>=25) {
      handleClickRight();
    }
  }

  useEffect(() => {
    ndc['arr'] = Array.from(ndc.carouselNav.children); 
  },[])
 
  useEffect(() => {
    getSlidePosition();
    moveTheSlide(slider);
    arrowShowHide(slider);
    setCarouselNav(slider);
  },[slider])
  

  useEffect(()=>{
    setSlider(carouselNav);
  },[carouselNav])

  return ( 
    
    <>
    <div className={style["page-wrapper"]}>
      
        <section className={style["content-wrapper"]}>
          <div className={style["container"]}>
            <div className={style["carousel"]}>
              <button ref={el => ndc['btnL']= el} className={`${style["carousel__button"]} ${style["carousel__button--left"]}`} onClick={() => handleClickLeft()}><i className={`fas fa-chevron-left ${style["carousel__arrow-color"]}`} aria-label="previous-image"></i></button>
              <div className={style["carousel__track-container"]}>
                <ul ref={el=>ndc['track'] = el} className={style["carousel__track"]} onTouchStart={(e)=>touchStart(e)} onTouchEnd={(e)=>touchEnd(e)} onContextMenu={(e) => {e.preventDefault() 
                e.stopPropagation()}}>
                  {details.photo.map((data,index) => {
                  return <li key={`${index}`} ref={el=> ndc[`li${index}`] = el} className={style["carousel__slide"]}><div className={style["carousel__image-wrapper"]}><Image  src={data.name} alt={data.alt} width={1280} height={720} layout='intrinsic' priority/></div></li>
                  })}
                </ul>
              </div>
              <button ref={el => ndc['btnR'] = el} className={`${style["carousel__button"]} ${style["carousel__button--right"]}`} 
                    onClick={() => handleClickRight()} ><i className={`fas fa-chevron-right ${style["carousel__arrow-color"]}`}  aria-label="next-image"></i></button>
            </div>
              <div ref={el=> ndc['carouselNav']=el} className={style["carousel__nav"]} onClick={(e)=>pickSlide(e)}>
                {details.photo.map((data,index)=> {
                  return <button key={`${index}`} type='button' aria-label="change-image" ref={el=>ndc[`btn${index}`]=el} className={`${style["carousel__nav-button"]} ${(carouselNav===index)? style["nav-indicator"]: ""}`} ><Image src={data.name} alt={data.alt} width={100} height={56.25} layout='intrinsic' /></button>
                })}
              </div>
            <article className={style["information"]}>
              <Link href={`/products/${details.category}`}><a className={style["information-link"]}>{details.categoryName}</a></Link>
              <h2 className={style["information-title"]}>{`${details.id}`}</h2>
              <p className={style["information-text"]}>{details.text}</p>
              <Image src={`${details.blackPic}`} width='400' height='225' layout='intrinsic' alt={details.blackPicAlt}/>
              <ul className={style["product-details-list"]}>
                <li className={style["product-details-list-item"]}><b>Μήκος:</b> {details.leng}</li>
                <li className={style["product-details-list-item"]}><b>Πλάτος:</b> {details.width}</li>
                <li className={style["product-details-list-item"]}><b>Ύψος:</b> {details.height}</li>
                <li className={style["product-details-list-item"]}><b>Βαρος:</b> {details.weight}</li>
              </ul>
            </article>
          </div>
        </section>
      
    </div>
    </>
   );

   
}
 
export default FurnitureTemplate;
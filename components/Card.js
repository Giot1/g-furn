import styles from '../styles/CardList.module.css'
import Image from 'next/image'

const Card = ({ data}) => {
  return (  
    <>
        <div className={styles["card-item"]}>
        <Image src={data.photo} alt={data.alt} width={350} height={350} layout="intrinsic" />
        <h2 className={styles["item-title"]}><span className={styles["line"]}></span>{data.id}</h2>
        </div>
    </>
  );
}
 
export default Card;